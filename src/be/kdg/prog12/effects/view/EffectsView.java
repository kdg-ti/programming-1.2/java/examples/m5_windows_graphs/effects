package be.kdg.prog12.effects.view;

import javafx.scene.control.Label;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;

public class EffectsView extends BorderPane {
private Label text;
	public EffectsView() {
		initialiseNodes();
		layoutNodes();
	}

	private void initialiseNodes() {
		text = new Label("Narcissus");
	}
	private void layoutNodes() {
		text.setFont(new Font(100));
		text.setEffect(new Reflection());
		this.setCenter(text);
		this.setPrefSize(500,500);
	}


}
