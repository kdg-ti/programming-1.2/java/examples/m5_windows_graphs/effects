package be.kdg.prog12.effects.view;

import be.kdg.prog12.effects.model.Effects;
import javafx.stage.Window;

public class EffectsPresenter {
		private Effects model;
		private EffectsView view;

		public EffectsPresenter(Effects model, EffectsView view) {
			this.model = model;
			this.view = view;
			addEventHandlers();
			updateView();
		}

		private void addEventHandlers() { }

		private void updateView() {/* fills the view with model data*/}

	public void addWindowEventHandlers() {
			Window window = view.getScene().getWindow();
			// add window event handlers
	}
}
