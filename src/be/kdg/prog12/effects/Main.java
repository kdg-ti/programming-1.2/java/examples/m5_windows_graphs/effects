package be.kdg.prog12.effects;

import be.kdg.prog12.effects.model.Effects;
import be.kdg.prog12.effects.view.EffectsPresenter;
import be.kdg.prog12.effects.view.EffectsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		Effects model = new Effects();
		EffectsView view = new EffectsView();
		EffectsPresenter presenter = new EffectsPresenter(model, view);
		primaryStage.setScene(new Scene(view));
		presenter.addWindowEventHandlers();
		primaryStage.show();
	}
}